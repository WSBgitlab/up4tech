# App commands
dev:
	npm run-script dev
build:
	npm run-script build
test:
	npm run-script test
# Deploy commands
docker-up:
	docker-compose up -d
docker-down:
	docker-compose down