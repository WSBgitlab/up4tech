import React from "react";
import { BsShieldFillCheck } from "react-icons/bs";
import { BiSearchAlt } from "react-icons/bi";
import { FcAdvertising } from "react-icons/fc"

const ServiceCard = ({ color, title, icon, subtitle }) => (
  <div className="flex flex-row justify-start items-start w-full white-glassmorphism p-3 m-2 cursor-pointer hover:shadow-xl">
    <div className={`w-10 h-10 rounded-full flex justify-center items-center ${color}`}>
      {icon}
    </div>
    <div className="ml-5 flex flex-col flex-1">
      <h3 className="mt-2 text-white text-lg">{title}</h3>
      <p className="mt-1 text-white text-sm md:w-9/12">
        {subtitle}
      </p>
    </div>
  </div>
);

const Services = () => (
  <div className="flex w-full justify-center items-center gradient-bg-services">
    <div className="flex mf:flex-row flex-col items-center justify-between md:p-20 py-12 px-4">
      <div className="flex-1 flex flex-col justify-start items-start">
        <h1 className="text-white text-3xl sm:text-5xl py-2 text-gradient ">
          Venha fazer parte da transformação em investimentos.
          <br />
        </h1>
        <p className="text-left my-2 text-white font-light md:w-9/12 w-11/12 text-base">
          Temos uma plataforma para te ajudar a gerenciar seus ativos de forma inteligente e sofisticada,
          produtos e serviços ao seu dispor com qualidade e segurança.
        </p>
      </div>

      <div className="flex-1 w-100 flex flex-col justify-start items-center">
        <ServiceCard
          color="bg-[#2952E3]"
          title="Segurança garantida"
          icon={<BsShieldFillCheck fontSize={21} className="text-white" />}
          subtitle="Segurança é o nosso principal foco, venha trazer seu dinheiro e transformar seus investimentos."
        />
        <ServiceCard
          color="bg-[#e33030]"
          title="Melhores investimentos"
          icon={<BiSearchAlt fontSize={21} className="text-white" />}
          subtitle="Mantendo a qualidade e segurança. Privacidade em investimentos com alta performance!"
        />
        <ServiceCard
          color="bg-[#FFF]"
          title="Rapidez na tomada de decisão"
          icon={<FcAdvertising fontSize={21} className="text-white" />}
          subtitle="Temos a maior plataforma de investimentos automatizados. Sempre com garantia para seu dinheiro render ainda mais!"
        />
      </div>
    </div>
  </div>
);

export default Services;
