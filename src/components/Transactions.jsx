import React, { useContext } from "react";

import { TransactionContext } from "../context/TransactionContext";

import companies from "../utils/companies";

const TransactionsCard = ({ addressTo, name, setor, addressFrom, timestamp, message, keyword, amount, url }) => {
  return (
    <div className="m-4 flex flex-1
      2xl:min-w-[450px]
      2xl:max-w-[500px]
      sm:min-w-[270px]
      sm:max-w-[250px]
      min-w-full
      flex-col p-3 rounded-md hover:shadow-2xl"
    >
      <div className="flex flex-col items-center w-full mt-3">
        <div className="display-flex justify-start w-10/12 bg-[#fff] mb-6 p-2 text-sm rounded-sm border-2 border-[#26325721] shadow-lg">
          <p >
            <a className="underline decoration-sky-500">Empresa: {name}</a>
          </p>
          <p>
            <p className="text-[#1b2137] text-sm">Setor: {setor}</p>
          </p>
          <p className="text-[#1b2137] text-sm">Valor: {amount} BRL</p>
          {message && (
            <>
              <br />
              <p className="text-[#1b2137] text-sm">Message: {message}</p>
            </>
          )}
        </div>
        <img
          src={url}
          alt="nature"
          className="w-10/12 hover:shadow-2xl bg-[#fff] h-50 2xl:h-80 rounded-sm shadow-lg object-cover border"
        />
      </div>
    </div>
  );
};

const Transactions = () => {
  const { transactions, currentAccount } = useContext(TransactionContext);

  return (
    <div className="flex w-full justify-center items-center 2xl:px-20 gradient-bg-transactions">
      <div className="flex flex-col md:p-12 py-12 px-4">
        {currentAccount ? (
          <h3 className="text-white text-2xl text-center my-2">
            Latest Transactions
          </h3>
        ) : (
          <h3 className="text-white text-3xl text-center my-2">
            Atualizações do mercado no momento atual.
          </h3>
        )}

        <div className="flex flex-wrap justify-center items-center mt-5">
          {[...companies, ...transactions].reverse().map((transaction, i) => (
            <TransactionsCard key={i} {...transaction} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Transactions;
